const Air = () => {
    return <div>This is Air Quality page</div>;
};

const Default = () => {
    return <div>This is Default page</div>;
};

const Energy = () => {
    return (<div>Hello <br></br>Hello COmes here <br></br>This is Energy Monitoring page</div>);
};

const Solar = () => {
    return <div>This is Solar page</div>;
};

const SmartRooms = () => {
    return <div>This is Smart Rooms page</div>;
};

const Weather = () => {
    return <div>This is Weather Monitoring Page</div>;
};

const Water = () => {
    return <div>This is Water Monitoring page</div>;
};

const Crowd = () => {
    return <div>This is Crowd Monitoring page</div>;
};

const Wisun = () => {
    return <div>This is Wisun page</div>;
};

export {Default,Air,Energy,Solar,SmartRooms,Weather,Water,Crowd,Wisun};